"use strict";

const { Adw, Gio, GLib, GObject, Gtk } = imports.gi;

const ExtensionUtils = imports.misc.extensionUtils;
const Me = ExtensionUtils.getCurrentExtension();

const INDEXES = [
  "S&P/ASX 20",
  "S&P/ASX 50",
  "S&P/ASX 100",
  "S&P/ASX 200",
  "S&P/ASX 300",
];

function init() {}

function fillPreferencesWindow(window) {
  // log(`${Me.metadata.name}: fillPreferencesWindow`);
  // Use the same GSettings schema as in `extension.js`
  const settings = ExtensionUtils.getSettings(
    "org.gnome.shell.extensions.asx-tracker"
  );

  // Create a preferences page and group
  const page = new Adw.PreferencesPage();
  const group = new Adw.PreferencesGroup();
  page.add(group);

  // Create a new preferences row
  const rowMenuLabel = new Adw.ActionRow({
    title: "Menu index",
    subtitle: "Which index to show in the menu bar",
  });
  group.add(rowMenuLabel);

  // Create the drop down and bind its value to the `menu-index` key
  // const storeIndex = new Gtk.ListStore(GObject.TYPE_STRING, GObject.TYPE_STRING)
  // storeIndex.append([1, "Test"])
  // const fieldMenuIndex = Gtk.ComboBox.new_with_model_and_entry(storeIndex);
  const storeIndex = new Gtk.StringList();
  INDEXES.forEach((index) => storeIndex.append(index));
  const fieldMenuIndex = new Gtk.DropDown({
    model: storeIndex,
    valign: Gtk.Align.CENTER,
  });
  // fieldMenuIndex.connect("activate", (entry) => {
  //   log(`activate: ${entry}`)
  // });
  settings.bind(
    "menu-index",
    fieldMenuIndex,
    "selected",
    Gio.SettingsBindFlags.DEFAULT
  );

  // Add the field to the row
  rowMenuLabel.add_suffix(fieldMenuIndex);
  rowMenuLabel.activatable_widget = fieldMenuIndex;

  // Create a new preferences row
  const rowShareCodes = new Adw.ActionRow({
    title: "Share codes",
    subtitle: "ASX codes for shares to track",
  });
  group.add(rowShareCodes);

  // Create the entry and connect its value to the `share-codes` key
  let fieldShareCodes = new Gtk.Entry({
    valign: Gtk.Align.CENTER,
  });
  fieldShareCodes.set_text(
    settings.get_value("share-codes").deepUnpack().toString()
  );
  fieldShareCodes.connect("changed", (entry) => {
    let array = `[${entry
      .get_text()
      .split(",")
      .map((v) => `'${v}'`)}]`;
    const varType = new GLib.VariantType("as");
    let value = GLib.Variant.parse(varType, array, null, null);
    settings.set_value("share-codes", value);
  });

  // Add the field to the row
  rowShareCodes.add_suffix(fieldShareCodes);
  rowShareCodes.activatable_widget = fieldShareCodes;

  // Create a new preferences row
  const rowRefreshInterval = new Adw.ActionRow({
    title: "Refresh interval (s)",
  });
  group.add(rowRefreshInterval);

  // Create the spin and bind its value to the `refresh-interval` key
  const fieldRefreshInterval = new Gtk.SpinButton({
    valign: Gtk.Align.CENTER,
    adjustment: new Gtk.Adjustment({
      lower: 1,
      upper: 3600,
      step_increment: 1,
    }),
  });
  settings.bind(
    "refresh-interval",
    fieldRefreshInterval,
    "value",
    Gio.SettingsBindFlags.DEFAULT
  );

  // Add the field to the row
  rowRefreshInterval.add_suffix(fieldRefreshInterval);
  rowRefreshInterval.activatable_widget = fieldRefreshInterval;

  // Create a new preferences row
  const rowShowPrice = new Adw.ActionRow({
    title: "Show share price",
  });
  group.add(rowShowPrice);

  // Create the spin and bind its value to the `refresh-interval` key
  const fieldShowPrice = new Gtk.Switch({
    active: settings.get_boolean("show-price"),
    valign: Gtk.Align.CENTER,
  });
  settings.bind(
    "show-price",
    fieldShowPrice,
    "active",
    Gio.SettingsBindFlags.DEFAULT
  );

  // Add the field to the row
  rowShowPrice.add_suffix(fieldShowPrice);
  rowShowPrice.activatable_widget = fieldShowPrice;

  // Create a new preferences row
  const rowShowChangeInPercent = new Adw.ActionRow({
    title: "Show change in percent",
  });
  group.add(rowShowChangeInPercent);

  // Create the spin and bind its value to the `refresh-interval` key
  const fieldShowChangeInPercent = new Gtk.Switch({
    active: settings.get_boolean("show-change-in-percent"),
    valign: Gtk.Align.CENTER,
  });
  settings.bind(
    "show-change-in-percent",
    fieldShowChangeInPercent,
    "active",
    Gio.SettingsBindFlags.DEFAULT
  );

  // Add the field to the row
  rowShowChangeInPercent.add_suffix(fieldShowChangeInPercent);
  rowShowChangeInPercent.activatable_widget = fieldShowChangeInPercent;

  // Add our page to the window
  window.add(page);
}

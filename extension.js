const { Clutter, Gio, Soup, St } = imports.gi;

const Lang = imports.lang;
const Mainloop = imports.mainloop;
const ExtensionUtils = imports.misc.extensionUtils;
const Me = ExtensionUtils.getCurrentExtension();
const Util = imports.misc.util;
const Main = imports.ui.main;
const PanelMenu = imports.ui.panelMenu;
const PopupMenu = imports.ui.popupMenu;

const DOWN_ARROW = new Gio.ThemedIcon({ name: "go-down-symbolic" });
const UP_ARROW = new Gio.ThemedIcon({ name: "go-up-symbolic" });

const INDEXES = [
  "S&P/ASX 20",
  "S&P/ASX 50",
  "S&P/ASX 100",
  "S&P/ASX 200",
  "S&P/ASX 300",
];

class Extension {
  constructor() {
    this._indicator = null;
    this._timeout = null;
    this._label = null;
    this._icon = null;
    this._settings = null;
    this._menuIndex = null;
    this._resetMenuIndex = this._resetMenuIndex.bind(this);
    this._shareCodes = null;
    this._resetShareCodes = this._resetShareCodes.bind(this);
    this._refreshInterval = null;
    this._resetRefreshInterval = this._resetRefreshInterval.bind(this);
    this._shareValueLabels = {};
    this._httpSession = new Soup.Session();
  }

  enable() {
    log(`enabling ${Me.metadata.name}`);
    // Get settings
    this._settings = ExtensionUtils.getSettings(
      "org.gnome.shell.extensions.asx-tracker"
    );

    let indicatorName = `${Me.metadata.name} Indicator`;

    // Create a panel button
    this._indicator = new PanelMenu.Button(0.0, indicatorName, false);

    // Add a label
    const hBox = new St.BoxLayout({ style_class: "panel-status-menu-box" });
    this._label = new St.Label({
      text: Me.metadata.name,
      y_align: Clutter.ActorAlign.CENTER,
      style: "margin-left: 4px;",
    });
    this._icon = new St.Icon({
      gicon: null,
      icon_size: 12,
      y_align: Clutter.ActorAlign.CENTER,
    });
    hBox.add_child(this._icon);
    hBox.add_child(this._label);
    this._indicator.add_child(hBox);

    // Connect settings
    this._menuIndex = INDEXES[this._settings.get_int("menu-index")];
    this._settings.connect("changed::menu-index", this._resetMenuIndex);
    this._shareCodes = this._settings.get_value("share-codes").deepUnpack();
    this._settings.connect("changed::share-codes", this._resetShareCodes);
    this._refreshInterval = this._settings.get_int("refresh-interval");
    this._settings.connect(
      "changed::refresh-interval",
      this._resetRefreshInterval
    );

    // Create menu
    this._buildMenu();

    // Setup timeout
    this._setupTimeout();

    // `Main.panel` is the actual panel you see at the top of the screen,
    // not a class constructor.
    Main.panel.addToStatusArea(indicatorName, this._indicator);
  }

  // REMINDER: It's required for extensions to clean up after themselves when
  // they are disabled. This is required for approval during review!
  disable() {
    // log(`disabling ${Me.metadata.name}`);
    this._indicator.destroy();
    this._indicator = null;

    // Destroy timeout
    this._clearTimeout();
  }

  _resetMenuIndex() {
    // log(`${Me.metadata.name}: _resetMenuIndex`);
    this._menuIndex = INDEXES[this._settings.get_int("menu-index")];

    // Refresh data
    this._refreshData();
  }

  _resetRefreshInterval() {
    // log(`${Me.metadata.name}: _resetRefreshInterval`);
    this._refreshInterval = this._settings.get_int("refresh-interval");

    // Reset timeout
    this._clearTimeout();
    this._setupTimeout();
  }

  _resetShareCodes() {
    this._shareCodes = this._settings.get_value("share-codes").deepUnpack();

    // Rebuild menu
    this._buildMenu();
    this._refreshData();
  }

  _openSettings() {
    // log(`${Me.metadata.name}: _openSettings`);
    if (typeof ExtensionUtils.openPrefs === "function") {
      ExtensionUtils.openPrefs();
    } else {
      Util.spawn(["gnome-shell-extension-prefs", Me.uuid]);
    }
  }

  _buildMenu() {
    // log(`${Me.metadata.name}: _buildMenu`);
    // Clear out existing menu items
    this._indicator.menu.removeAll();
    // Create menu item for each share code
    this._shareCodes.forEach(function (shareCode) {
      // Create clickable menu item
      let menuItem = new PopupMenu.PopupMenuItem(shareCode);
      menuItem.label.set_style("font-weight: bold;");
      menuItem.connect(
        "activate",
        Lang.bind(menuItem, () => {
          const url = "https://www2.asx.com.au/markets/etp/" + shareCode;
          Util.spawn(["firefox", url]);
        })
      );

      // Create data boxes
      const vBox = new St.BoxLayout({
        vertical: true,
        x_align: Clutter.ActorAlign.END,
        x_expand: true,
      });
      const hBox = new St.BoxLayout({
        x_align: Clutter.ActorAlign.END,
        x_expand: true,
      });
      let icon = new St.Icon({
        gicon: null,
        icon_size: 12,
        y_align: Clutter.ActorAlign.CENTER,
      });
      let priceLabel = new St.Label({
        text: "...",
        x_align: Clutter.ActorAlign.END,
        x_expand: true,
      });
      let changeInPercentLabel = new St.Label({
        text: "...",
        y_align: Clutter.ActorAlign.CENTER,
        x_align: Clutter.ActorAlign.END,
        x_expand: true,
        style: "margin-left: 4px;",
      });
      this._settings.bind(
        "show-price",
        priceLabel,
        "visible",
        Gio.SettingsBindFlags.DEFAULT
      );
      this._settings.bind(
        "show-change-in-percent",
        hBox,
        "visible",
        Gio.SettingsBindFlags.DEFAULT
      );
      vBox.add_child(priceLabel);
      hBox.add_child(icon);
      hBox.add_child(changeInPercentLabel);
      vBox.add_child(hBox);
      menuItem.actor.add_child(vBox);

      // Store links to label to enable simple updating via shareCode
      this._shareValueLabels[shareCode] = {
        icon: icon,
        price: priceLabel,
        changeInPercent: changeInPercentLabel,
      };

      // Add menu item to menu
      this._indicator.menu.addMenuItem(menuItem);
    }, this);

    // Add separator
    this._indicator.menu.addMenuItem(new PopupMenu.PopupSeparatorMenuItem());

    // Add 'Settings' menu item to open settings
    let settingsMenuItem = new PopupMenu.PopupMenuItem("Settings");
    this._indicator.menu.addMenuItem(settingsMenuItem);
    settingsMenuItem.connect("activate", this._openSettings);
  }

  _updateButtonLabel(changeInPrice, changeInPercent) {
    // log(`${Me.metadata.name}: _updateButtonLabel`);
    this._label.set_text(`${changeInPercent.replace("-", "")}`);
    this._icon.set_gicon(
      changeInPrice < 0 ? DOWN_ARROW : changeInPrice > 0 ? UP_ARROW : null
    );
    // this._icon.set_style_class_name(
    //   changeInPrice < 0
    //     ? "price-decreased"
    //     : changeInPrice > 0
    //     ? "price-increased"
    //     : ""
    // );
  }

  _updateShareCodeLabels(shareCode, price, changeInPrice, changeInPercent) {
    // log(`${Me.metadata.name}: _updateShareCodeLabels`);
    this._shareValueLabels[shareCode]["price"].set_text(`${price}`);
    this._shareValueLabels[shareCode]["changeInPercent"].set_text(
      `${changeInPercent.replace("-", "")}`
    );
    this._shareValueLabels[shareCode]["icon"].set_gicon(
      changeInPrice < 0 ? DOWN_ARROW : changeInPrice > 0 ? UP_ARROW : null
    );
    this._shareValueLabels[shareCode]["icon"].set_style_class_name(
      changeInPrice < 0
        ? "price-decreased"
        : changeInPrice > 0
        ? "price-increased"
        : ""
    );
  }

  _sendRequest(url, type = "GET") {
    // log(`${Me.metadata.name}: _sendRequest`);
    let message = Soup.Message.new(type, url);
    let responseCode = this._httpSession.send_message(message);
    if (responseCode !== 200) return;
    try {
      return JSON.parse(message["response-body"].data);
    } catch (err) {
      logError(err);
    }
  }

  _refreshData() {
    // log(`${Me.metadata.name}: _refreshData`);
    // Update ASX index info
    try {
      const url = "https://www.asx.com.au/asx/1/index-info";
      const resp = this._sendRequest(url);
      let index = resp.find((obj) => obj["name_full"] == this._menuIndex);
      this._updateButtonLabel(
        index["index_change"],
        index["change_in_percent"]
      );
    } catch (err) {
      logError(err);
    }

    // Update shares
    this._shareCodes.forEach(function (shareCode) {
      try {
        const url = "https://www.asx.com.au/asx/1/share/" + shareCode;
        const resp = this._sendRequest(url);
        this._updateShareCodeLabels(
          shareCode,
          resp["last_price"],
          resp["change_price"],
          resp["change_in_percent"]
        );
      } catch (err) {
        logError(err);
      }
    }, this);
  }

  _setupTimeout() {
    // log(`${Me.metadata.name}: _setupTimeout`);
    this._refreshData();
    this._timeout = Mainloop.timeout_add_seconds(this._refreshInterval, () => {
      this._refreshData();
      return true;
    });
  }

  _clearTimeout() {
    // log(`${Me.metadata.name}: _clearTimeout`);
    if (!this._timeout) return;

    Mainloop.source_remove(this._timeout);
    this._timeout = null;
  }
}

function init() {
  // log(`initializing ${Me.metadata.name}`);
  return new Extension();
}
